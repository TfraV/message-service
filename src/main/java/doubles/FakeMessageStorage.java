package doubles;

import exception.NoMessageFoundException;
import exception.NoMessagesInPositionException;
import exception.UnauthorizedException;
import model.MessageRecord;
import model.MessageStorage;
import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.ZonedDateTime;
import java.util.*;

public class FakeMessageStorage implements MessageStorage {
    private static final Logger logger = LoggerFactory.getLogger(FakeMessageStorage.class);
    private Map<String, List<MessageRecord>> storage;

    public FakeMessageStorage() {
        this.storage = new HashMap<String, List<MessageRecord>>();
    }

    public boolean bIsHealthy()
    {
        return true;
    }

    @Override
    public MessageRecord addMessage(String positionString, MessageRecord messageRecord) {
        // get messages in position
        List<MessageRecord> messagesInPosition = storage.get(positionString);

        if (messagesInPosition == null) {
            logger.warn("positionString not found - creating location at the moment");
            // hvordan finder vi ud af om den pågældende position findes? Skal man kalde CaveService?

            messagesInPosition = new ArrayList<MessageRecord>();

            // men hvis den ikke findes skal der smides en 404
        }

        // *** create message part ***
        String id = UUID.randomUUID().toString();

        messageRecord.setId(id);
        messageRecord.setCreatorTimeStampISO8601(ZonedDateTime.now());

        // add message to list of messages in position
        messagesInPosition.add(messageRecord);
        storage.put(positionString, messagesInPosition);

        logger.info("number of messages in position '{}' is {}", positionString, messagesInPosition.size());

        logger.info("Message created: {}", messageRecord);

        return messageRecord;
    }

    @Override
    public MessageRecord updateMessage(String positionString, String messageId, MessageRecord newMessageRecord) throws Exception {
        List<MessageRecord> messagesInPosition = null;
        MessageRecord messageWithId = null;

        messagesInPosition = getMessagesInPosition(positionString);
        messageWithId = getMessageWithId(messagesInPosition, messageId);

        validateCreator(newMessageRecord, messageWithId);

        logger.info("Updating message: {}", messageWithId);

        messageWithId.setContents(newMessageRecord.getContents());

        updateMessageInCorrectPosition(positionString, messageWithId, messagesInPosition);

        return messageWithId;
    }

    private void updateMessageInCorrectPosition(String positionString, MessageRecord updatedMessageRecord, List<MessageRecord> messagesInPosition) {
        // update message list and messages in position
        int i = 0;
        for (MessageRecord messageRecord : messagesInPosition) {
            if (messageRecord.getId().equals(updatedMessageRecord.getId())) {
                break;
            }

            i++;
        }

        messagesInPosition.set(i, updatedMessageRecord);
        storage.put(positionString, messagesInPosition);
        logger.info("Message updated: {}", updatedMessageRecord);
    }

    @Override
    public List<MessageRecord> getMessageList(String positionString, int startIndex, int pageSize) throws NoMessagesInPositionException {
        List<MessageRecord> messagesInPosition = getMessagesInPosition(positionString);

        int size = messagesInPosition.size();
        int firstIndex = startIndex > size ? size : startIndex;
        int lastIndex = startIndex + pageSize > size ? size : startIndex + pageSize;

        //Reverse the elements, so the one posted last is send first etc.
        List<MessageRecord>  messagesInPositionReverseList = new ArrayList<MessageRecord>();
        for(int i = messagesInPosition.size() - 1; i >= 0; i--)
        {
            messagesInPositionReverseList.add(messagesInPosition.get(i));
        }

        return messagesInPositionReverseList.subList(firstIndex, lastIndex);
    }

    private List<MessageRecord> getMessagesInPosition(String positionString) throws NoMessagesInPositionException {
        // get messages in position
        List<MessageRecord> messagesInPosition = storage.get(positionString);

        if (messagesInPosition == null) {
            logger.error("positionString not found");

            throw new NoMessagesInPositionException("positionString not found");
        }

        if (messagesInPosition.size() == 0) {
            throw new NoMessagesInPositionException("No messages found in position " + positionString);
        }

        return messagesInPosition;
    }

    private MessageRecord getMessageWithId(List<MessageRecord> messagesInPosition, String id) throws NoMessageFoundException {
        MessageRecord messageWithId = null;

        for (MessageRecord messageRecord : messagesInPosition) {
            if (messageRecord.getId().equals(id)) {
                messageWithId = messageRecord;
                break;
            }
        }

        if (messageWithId == null) {
            logger.error("Message with id: {} not found", id);

            throw new NoMessageFoundException(String.format("Message with id: %s not found", id));
        }

        return messageWithId;
    }

    private static void validateCreator(MessageRecord newMessageRecord, MessageRecord existingRecord) throws UnauthorizedException {
        // validate that creator is the same
        if (!newMessageRecord.getCreatorId().equals(existingRecord.getCreatorId())) {
            logger.error("CreatorId not the same. Existing: {} and requested: {}", existingRecord.getCreatorId(), newMessageRecord.getCreatorId());

            throw new UnauthorizedException(String.format("CreatorId not the same. Existing: %s and requested: %s", existingRecord.getCreatorId(), newMessageRecord.getCreatorId()));
        }
    }
}
