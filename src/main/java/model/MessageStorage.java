package model;

import exception.NoMessagesInPositionException;

import java.util.List;

public interface MessageStorage {
    public MessageRecord addMessage(String positionString, MessageRecord messageRecord);
    public MessageRecord updateMessage(String positionString, String messageId, MessageRecord newMessageRecord) throws Exception;
    public List<MessageRecord> getMessageList(String positionString, int startIndex, int pageSize) throws NoMessagesInPositionException;
    public boolean bIsHealthy();
}
