package model;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class MessageRecord {
    private final String creatorId;
    private final String creatorName;
    private String contents;
    private String creatorTimeStampISO8601;
    private String id;

    public MessageRecord(String contents, String creatorId, String creatorName) {
        this.contents = contents;
        this.creatorId = creatorId;
        this.creatorName = creatorName;
        this.creatorTimeStampISO8601 = "none";
        this.id = "none";
    }

    public String getCreatorId() {
        return creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getCreatorTimeStampISO8601() {
        return creatorTimeStampISO8601;
    }

    public void setCreatorTimeStampISO8601(String creatorTimeStampISO8601) {
        this.creatorTimeStampISO8601 = creatorTimeStampISO8601;
    }

    public void setCreatorTimeStampISO8601(ZonedDateTime timestamp) {
        setCreatorTimeStampISO8601(timestamp.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "MessageRecord{" +
                "creatorId='" + creatorId + '\'' +
                ", creatorName='" + creatorName + '\'' +
                ", contents='" + contents + '\'' +
                ", creatorTimeStampISO8601='" + creatorTimeStampISO8601 + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
