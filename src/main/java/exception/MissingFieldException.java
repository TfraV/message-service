package exception;

public class MissingFieldException extends BadRequestException {
    public MissingFieldException(String message) {
        super(message);
    }

    public MissingFieldException(String message, Throwable cause) {
        super(message, cause);
    }
}
