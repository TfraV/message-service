package exception;

public class MissingQueryParameterException extends BadRequestException {
    public MissingQueryParameterException(String message) {
        super(message);
    }

    public MissingQueryParameterException(String message, Throwable cause) {
        super(message, cause);
    }
}
