package exception;

public class NoMessagesInPositionException extends NotFoundException {
    public NoMessagesInPositionException(String message) {
        super(message);
    }

    public NoMessagesInPositionException(String message, Throwable cause) {
        super(message, cause);
    }
}
