package exception;

public class NoMessageFoundException extends NotFoundException {
    public NoMessageFoundException(String message) {
        super(message);
    }

    public NoMessageFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
