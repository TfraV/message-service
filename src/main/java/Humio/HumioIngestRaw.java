package Humio;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.time.ZonedDateTime;

import static java.time.ZonedDateTime.now;

public class HumioIngestRaw
{
    private String sHumioUrl;
    private String sHumioToken;

    public HumioIngestRaw()
    {
        sHumioUrl = "https://cloud.community.humio.com";
        sHumioToken = "001630d7-9517-428a-886f-c5a2f53a3618";
    }

    public void info( String sLogMessage )
    {
        ZonedDateTime timeStamp = now();
        bSendLogToHumio( timeStamp.toString() + " " + sLogMessage );
    }

    private boolean bSendLogToHumio( String sKeyValueLog )
    {
        try
        {
            HttpRequest clRequest = HttpRequest.newBuilder(URI.create(sHumioUrl +  "/api/v1/ingest/raw"))
                    .header("accept", "application/json")
                    .header("authorization", "Bearer " + sHumioToken)
                    .timeout(Duration.ofSeconds(5)) //Connect timeout
                    .POST(HttpRequest.BodyPublishers.ofString(sKeyValueLog))
                    .build();

            HttpClient client = HttpClient.newBuilder().connectTimeout(Duration.ofSeconds(5)).build(); //Connection timeout
            HttpResponse<String> response = client.send(clRequest, HttpResponse.BodyHandlers.ofString());

            if( response.statusCode() == HttpServletResponse.SC_OK )
                return true;
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        return false;
    }


    //curl $YOUR_HUMIO_URL/api/v1/ingest/raw \
    //    -X POST \
    //    -H "Authorization: Bearer $INGEST_TOKEN" \
    //    -d 'My raw Message generated at "2016-06-06T12:00:00+02:00"'
}
